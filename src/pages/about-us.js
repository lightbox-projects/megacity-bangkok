import * as React from "react"
import { graphql } from "gatsby"

import Layout from "../components/layout"
import Seo from "../components/seo"

import Jumbotron from "../components/AboutUs/Jumbotron"

import { StaticImage } from "gatsby-plugin-image"
import { Trans } from 'react-i18next'
import { isMobile } from 'react-device-detect';

const AboutUs = ({ data, location }) => {
    const siteTitle = data.site.siteMetadata.title
    let featured_properties = data.featured_properties.nodes

    function handleContact() {
        window.open(`https://wa.me/66900935365`)
    }

    return (
        <Layout location={location} title={siteTitle} featured={featured_properties}>
            <Jumbotron />

            <div className="position-relative mt-4">
                <StaticImage src={'../../static/assets/asset-landing-4.png'} alt='Quotes' className='w-100'
                    style={{ aspectRatio: 'unset', height: 'unset', objectFit: 'cover', height: isMobile ? '500px' : '' }} />
                <div className="position-absolute d-flex w-100 h-100" style={{ top: 0, left: 0 }}>
                    <div className="m-auto p-3 container-lg">
                        <div className="h3 mb-0 text-white fw-normal text-center" style={{ lineHeight: isMobile ? '30px' : '' }}>
                            <Trans>we're more than just a property listing company. We're your trusted companion on the exciting journey of finding your perfect home. Whether you prefer a sleek urban apartment or a serene suburban retreat. Your Trusted Property Companion</Trans>
                        </div>
                    </div>
                </div>
            </div>

            <div className="bg-secondary py-5">
                <div className="container-lg">
                    <div className="row" style={{ gap: '10px 0' }}>
                        <div className="col-lg-6">
                            <div className="h1 fw-bolder text-white"><Trans>Our Services</Trans></div>
                            <div className="h3 fw-bolder text-white mt-5">
                                <i className="ti ti-circle-check me-3"></i>
                                <Trans>Consultation on Property Investment</Trans>
                            </div>
                            <div className="h3 fw-bolder text-white mt-5">
                                <i className="ti ti-circle-check me-3"></i>
                                <Trans>Legalization</Trans>
                            </div>
                            <div className="h3 fw-bolder text-white mt-5">
                                <i className="ti ti-circle-check me-3"></i>
                                <Trans>Site Tour</Trans>
                            </div>
                            <div className="h3 fw-bolder text-white mt-5">
                                <i className="ti ti-circle-check me-3"></i>
                                <Trans>FET Form</Trans>
                            </div>
                            <div className="h3 fw-bolder text-white mt-5">
                                <i className="ti ti-circle-check me-3"></i>
                                <Trans>Ownership Transferring</Trans>
                            </div>
                        </div>
                        <div className="col-lg-6">
                            <StaticImage alt="asset-landing-5" src="../../static/assets/asset-landing-5.png" className="h-100" />
                        </div>
                    </div>


                    <div className='row mt-5' style={{ gap: '10px 0' }}>
                        <div className='col-lg-4'>
                            <div className="h-100 d-flex flex-column">
                                <StaticImage src="../../static/assets/Rectangle 691.png" className="w-100" alt="asset-landing-kiri" />
                                <div className='d-flex align-items-center justify-content-center my-5' style={{ gap: '10px 20px' }}>
                                    <h1 className='h3 mb-0 text-white text-center fw-bolder'><Trans>Consultation on Property Investment</Trans></h1>
                                </div>
                                <p className='text-white text-center mt-auto'>
                                    <Trans>Whether it is cleaning a clogged drain or replacing it, our men know what’s right and will offer you the best possible solution.</Trans>
                                </p>
                            </div>
                        </div>
                        <div className='col-lg-4'>
                            <div className="h-100 d-flex flex-column"> 
                                <StaticImage src="../../static/assets/Rectangle 692.png" className="w-100" alt="asset-landing-tengah" />
                                <div className='d-flex align-items-center justify-content-center my-5' style={{ gap: '10px 20px' }}>
                                    <h1 className='h3 mb-0 text-white text-center fw-bolder'><Trans>Legalization</Trans></h1>
                                </div>
                                <p className='text-white text-center mt-auto'>
                                    <Trans>Whether you need an emergency repair or you are upgrading your home looking for new line installations, we can help.</Trans>
                                </p>
                            </div>
                        </div>
                        <div className='col-lg-4'>
                            <div className="h-100 d-flex flex-column">
                                <StaticImage src="../../static/assets/Rectangle 690.png" className="w-100" alt="asset-landing-kanan" />
                                <div className='d-flex align-items-center justify-content-center my-5' style={{ gap: '10px 20px' }}>
                                    <h1 className='h3 mb-0 text-white text-center fw-bolder'>Ownership Transfering</h1>
                                </div>
                                <p className='text-white text-center mt-auto'>
                                    <Trans>From hotels to schools, we have the necessary equipment and training to provide top-quality services wherever you need them.</Trans>
                                </p>
                            </div>
                        </div>
                    </div>


                    <div className="text-center display-3 fw-bolder text-white" style={{ marginTop: '3em' }}>
                        <Trans>We’re here to help! Call Us Today!</Trans>
                    </div>
                    <div className="d-flex mt-4">
                        <button className="btn btn-secondary m-auto px-5" onClick={() => handleContact()}>
                            <Trans>Book A Consultation</Trans>
                        </button>
                    </div>
                </div>
            </div>

        </Layout>
    )
}

export const Head = () => <Seo title="About Us" />

export default AboutUs

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
    featured_properties: allMarkdownRemark(
      filter: {frontmatter: {is_featured: {eq: true}}, fileAbsolutePath: {regex: "/property/"}}
      limit: 4
      skip: 0
      sort: {frontmatter: {title: ASC}}
    ) {
      nodes {
        fields {
          slug
        }
        frontmatter {
          address
          banner_image
          bathrooms
          bedrooms
          car_port
          date(formatString: "DD MMMM YYYY")
          title
          total_area
          status
          housing_type
          is_featured
          state
        }
      }
    } 
  }
`
