import * as React from "react"
import { graphql } from "gatsby"

import Layout from "../components/layout"
import Seo from "../components/seo"

import Jumbotron from '../components/Landing/Jumbotron'
import SearchFunction from '../components/Landing/SearchFunction'
import ItemSwiper from '../components/Landing/ItemSwiper'
import PropertyType from '../components/Landing/PropertyType'
import Testimony from '../components/Landing/Testimony'
import Section3 from '../components/Landing/Section3'

const LandingPage = ({ data, location }) => {
  const siteTitle = data.site.siteMetadata?.title || `Title`

  let properties = data.properties.nodes
  let testimony = data.testimony.nodes

  return (
    <Layout location={location} title={siteTitle} featured={properties}>
      {/* <Trans>Hi people</Trans> */}

      <Jumbotron />
      <SearchFunction />
      <ItemSwiper properties={properties} />
      <Section3 />
      <section id="property">
        <PropertyType />
      </section>
      <Testimony testimony={testimony} />

    </Layout>
  )
}

export default LandingPage

/**
 * Head export to define metadata for the page
 *
 * See: https://www.gatsbyjs.com/docs/reference/built-in-components/gatsby-head/
 */
export const Head = () => <Seo title="Landing Page" />

export const pageQuery = graphql`
  query ($language: String!){
    site {
      siteMetadata {
        title
      }
    } 
    locales: allLocale(filter: {language: {eq: $language}}) {
      edges {
        node {
          ns
          data
          language
        }
      }
    }
    properties: allMarkdownRemark(
      filter: {frontmatter: {is_featured: {eq: true}}, fileAbsolutePath: {regex: "/property/"}}
      limit: 4
      skip: 0
      sort: {frontmatter: {title: ASC}}
    ) {
      nodes {
        fields {
          slug
        }
        frontmatter {
          address
          banner_image
          bathrooms
          bedrooms
          car_port
          date(formatString: "DD MMMM YYYY")
          title
          total_area
          status
          housing_type
          is_featured
          state
        }
      }
    } 
    testimony: allMarkdownRemark(
      filter: {fileAbsolutePath: {regex: "/testimony/"}}
    ) {
      nodes {
        frontmatter {
          name
          thumbnail
        }
        html
      }
    }
  } 
` 
