import * as React from "react"
import { graphql } from "gatsby"

import Layout from "../components/layout"
import Seo from "../components/seo"

const NotFoundPage = ({ data, location }) => {
  const siteTitle = data.site.siteMetadata.title
  let featured_properties = data.featured_properties.nodes

  return (
    <Layout location={location} title={siteTitle} featured={featured_properties}>
      <h1>404: Not Found</h1>
      <p>You just hit a route that doesn&#39;t exist... the sadness.</p>
    </Layout>
  )
}

export const Head = () => <Seo title="404: Not Found" />

export default NotFoundPage

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
    featured_properties: allMarkdownRemark(
      filter: {frontmatter: {is_featured: {eq: true}}, fileAbsolutePath: {regex: "/property/"}}
      limit: 4
      skip: 0
      sort: {frontmatter: {title: ASC}}
    ) {
      nodes {
        fields {
          slug
        }
        frontmatter {
          address
          banner_image
          bathrooms
          bedrooms
          car_port
          date(formatString: "DD MMMM YYYY")
          title
          total_area
          status
          housing_type
          is_featured
          state
        }
      }
    } 
  }
`
