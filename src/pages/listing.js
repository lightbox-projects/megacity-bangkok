import * as React from "react"
import { graphql } from "gatsby"

import Layout from "../components/layout"
import Seo from "../components/seo"

import Jumbotron from '../components/Listing/Jumbotron'
import SearchFunction from '../components/Landing/SearchFunction'
import ItemSwiper from '../components/Listing/ItemSwiper'

const Listing = ({ data, location }) => {
  const siteTitle = data.site.siteMetadata?.title || `Title`
  let properties = data.properties
  let featured_properties = data.featured_properties.nodes

  return (
    <Layout location={location} title={siteTitle} featured={featured_properties}>
      {/* <Trans>Hi people</Trans> */}

      <Jumbotron />
      <section id="search">
        <SearchFunction currentState={location.state} />
        <ItemSwiper location={location} properties={properties} />
      </section>

    </Layout>
  )
}

export default Listing

/**
 * Head export to define metadata for the page
 *
 * See: https://www.gatsbyjs.com/docs/reference/built-in-components/gatsby-head/
 */
export const Head = () => <Seo title="Listing" />

export const pageQuery = graphql`
  query ($language: String!){
    site {
      siteMetadata {
        title
      }
    } 
    locales: allLocale(filter: {language: {eq: $language}}) {
      edges {
        node {
          ns
          data
          language
        }
      }
    }
    properties : allMarkdownRemark(filter : {fileAbsolutePath: {regex: "/property/"}}, sort: {frontmatter: {title: ASC}}) {
      nodes {
          fields {
              slug
          }
          frontmatter {
              address
              banner_image
              bathrooms
              bedrooms
              car_port
              date(formatString: "DD MMMM YYYY")
              title
              total_area
              status
              housing_type
              housing_sub_type
              price
              property_size
              state
              is_featured
          }
      }
      pageInfo {
          totalCount
      }
    }
    featured_properties: allMarkdownRemark(
      filter: {frontmatter: {is_featured: {eq: true}}, fileAbsolutePath: {regex: "/property/"}}
      limit: 4
      skip: 0
      sort: {frontmatter: {title: ASC}}
    ) {
      nodes {
        fields {
          slug
        }
        frontmatter {
          address
          banner_image
          bathrooms
          bedrooms
          car_port
          date(formatString: "DD MMMM YYYY")
          title
          total_area
          status
          housing_type
          is_featured
          state
        }
      }
    } 
  }
` 
