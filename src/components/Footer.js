import React, { useEffect, useState } from "react";
import { Link } from 'gatsby'
import { StaticImage } from "gatsby-plugin-image"
import { isMobile } from 'react-device-detect';
import { Trans, useTranslation } from 'react-i18next'
import LanguageSwitcher from './LanguageSwitcher'

const Footer = ({ featured }) => {
    const { t } = useTranslation();

    // const [items, setItems] = useState([])
    // useEffect(() => {
    //     var query = `
    //         query {
    //             allMarkdownRemark(
    //                 filter: {frontmatter: {is_featured: {eq: true}}}
    //                 limit: 3
    //                 skip: 0
    //               ) {
    //                 nodes {
    //                   fields {
    //                     slug
    //                   }
    //                   frontmatter { 
    //                     title 
    //                   }
    //                 }
    //             }         
    //         }
    //     `;

    //     fetch('/__graphql', {
    //         method: 'POST',
    //         headers: {
    //             'Content-Type': 'application/json',
    //             'Accept': 'application/json',
    //         },
    //         body: JSON.stringify({
    //             query,
    //             variables: {}
    //         })
    //     })
    //         .then(r => r.json())
    //         .then(data => {
    //             if (data.data) {
    //                 setItems(data.data.allMarkdownRemark?.nodes)
    //             }
    //         });
    // }, [])

    const [state, setState] = React.useState({
        name: '',
        email: '',
        body: '',
    });

    const handleChange = (event, id) => {
        setState({ ...state, [id]: event.target.value });
    };

    function handleContact() {
        let subject = `MegacityBangkok, Contact from ${state.name}`
        window.open(`mailto:thawdarkhant@gmail.com?subject=${subject}&body=${state.body}`)
    }

    return (
        <div className="">
            <div className="bg-secondary py-5">
                <div className="container-lg py-3">

                    <div className={`d-flex align-items-center mb-5 ${isMobile ? 'justify-content-center' : ''}`} style={{ gap: '10px' }}>
                        <StaticImage src="../../static/assets/logo.png" alt="Logo Footer" style={{ width: isMobile ? '200px' : '100px', height: isMobile ? '200px' : '100px', objectFit: 'contain' }} />
                        {isMobile ?
                            null
                            :
                            <h1 className="fw-bolder display-3 text-white mb-0">megacitybangkok.com</h1>
                        }
                    </div>
                    <div className="row align-items-start" style={{ gap: '10px 0' }}>
                        <div className="col-lg-4 text-white">
                            <div className="w-100" style={{}}>
                                <div className="d-flex flex-column" style={{ gap: '5px' }}>
                                    <h1 className="fw-bolder h5 text-white"><Trans>Phone Numbers</Trans></h1>
                                    <h1 className="h6 fw-normal mb-0 text-white">+959762850296</h1>
                                    <h1 className="h6 fw-normal mb-0 text-white">+66900935365</h1>

                                    <h1 className="fw-bolder h5 mt-4 text-white"><Trans>Email</Trans></h1>
                                    <h1 className="h6 fw-normal mb-0 text-white">info@megacitybangkok.com</h1>

                                    <h1 className="fw-bolder h5 mt-4 text-white"><Trans>Address</Trans></h1>
                                    <h1 className="h6 fw-normal mb-0 text-white">10/95, Trendy Office Building, Sukhuvit Soi 13, Khloytoei, Wattana, Bangkok 10110</h1>
                                </div>
                                <LanguageSwitcher />
                            </div>
                        </div>
                        <div className={`col-lg-auto ${isMobile ? 'my-5' : ''}`} style={{ borderRight: isMobile ? '' : '1px solid white', borderLeft: isMobile ? '' : '1px solid white', maxWidth: isMobile ? '' : '200px' }}>
                            <div className="d-flex flex-column" style={{ gap: '10px' }}>
                                <h3 className="h6 fw-bolder text-white mb-0" ><Trans>Navigate</Trans></h3>
                                <div className="bg-white w-100" style={{ height: '2px' }}></div>
                                <Link to='/' className="text-white" style={{ textDecoration: 'unset' }}><Trans>Home</Trans></Link>
                                <Link to='/listing' className="text-white" style={{ textDecoration: 'unset' }}><Trans>Listing</Trans></Link>

                                <h3 className="h6 fw-bolder text-white mt-5 mb-0" ><Trans>Featured Property</Trans></h3>
                                <div className="bg-white w-100" style={{ height: '2px' }}></div>
                                {
                                    featured.map(node => (
                                        <Link key={'FOOTER_FEATURED' + node.fields.slug} to={'/detail/' + node.fields.slug.replace('/property/', '') + '#navbar'} className="text-white" style={{ textDecoration: 'unset' }}>{node.frontmatter.title}</Link>
                                    ))
                                }
                            </div>
                        </div>
                        <div className={`col-lg ${isMobile ? '' : 'px-5'}`}>
                            <div className="mb-1 d-flex flex-wrap justify-content-between" style={{ gap: '10px' }}>
                                <h2 className="text-white fw-bolder h5 mb-0"><Trans>Contact us here</Trans></h2>
                            </div>

                            <form id="form-footer">
                                <div className="row" style={{ gap: '10px 0' }}>
                                    <div className="col-lg-6">
                                        <input className="form-control" value={state.name} onChange={event => handleChange(event, 'name')} placeholder={t("Name")} id="footer_name" required="" />
                                    </div>
                                    <div className="col-lg-6">
                                        <input className="form-control" value={state.email} onChange={event => handleChange(event, 'email')} placeholder={t("Email")} id="footer_email" required="" />
                                    </div>
                                    <div className="col-lg-12">
                                        <textarea className="form-control" value={state.body} onChange={event => handleChange(event, 'body')}
                                            id="footer_desc" required=""
                                            placeholder={t("Ask your question and send it right away to our inbox")} rows="3"></textarea>
                                    </div>
                                </div>
                            </form>
                            <div className="d-flex mt-3">
                                <button style={{ gap: '10px' }} onClick={() => handleContact()} className="btn btn-primary px-5 d-flex align-items-center mt-1 ms-auto waves-effect waves-float waves-light">
                                    <Trans>Send</Trans> <i className="ti ti-send"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="w-100 d-flex align-items-center flex-wrap justify-content-between container-fluid py-3" style={{ gap: '10px' }}>
                <div className="h6 mb-0" style={{ fontSize: '14px' }}>
                    Copyright {new Date().getFullYear()} by MegaCity Bangkok. All Rights Reserved.
                </div>
                <div className="d-flex align-items-center" style={{ gap: '10px' }}>
                    <div className="d-flex bg-primary text-white rounded-circle" style={{ height: '30px', width: '30px' }}>
                        <i className="m-auto ti ti-brand-facebook" style={{ fontSize: '18px' }}></i>
                    </div>
                    <div className="d-flex bg-primary text-white rounded-circle" style={{ height: '30px', width: '30px' }}>
                        <i className="m-auto ti ti-brand-google" style={{ fontSize: '18px' }}></i>
                    </div>
                    <div className="d-flex bg-primary text-white rounded-circle" style={{ height: '30px', width: '30px' }}>
                        <i className="m-auto ti ti-brand-linkedin" style={{ fontSize: '18px' }}></i>
                    </div>
                    <div className="d-flex bg-primary text-white rounded-circle" style={{ height: '30px', width: '30px' }}>
                        <i className="m-auto ti ti-brand-twitter" style={{ fontSize: '18px' }}></i>
                    </div>
                    <div className="d-flex bg-primary text-white rounded-circle" style={{ height: '30px', width: '30px' }}>
                        <i className="m-auto ti ti-brand-youtube" style={{ fontSize: '18px' }}></i>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Footer