import React from 'react';
import { useI18next } from 'gatsby-plugin-react-i18next';
import { Trans } from 'react-i18next'

import { Link } from 'gatsby'

const LanguageSwitcher = () => {
  const { languages, changeLanguage, language } = useI18next();

  let predefined_languages = {
    en: 'English',
    th: 'Thailand',
    id: 'Indonesia'
  }

  return (
    // <li className="nav-item dropdown">
    //   <Link className="nav-link dropdown-toggle" to={'/'} role="button" data-bs-toggle="dropdown" aria-expanded="false">
    //     <Trans>Languages</Trans>
    //   </Link>
    //   <ul className="dropdown-menu">
    //     {languages.map((language) => (
    //       <li key={language}>
    //         <Link style={{ cursor: 'pointer' }} key={language} onClick={(e) => {
    //           e.preventDefault();
    //           changeLanguage(language);
    //         }} className="dropdown-item" to={'/'}>{predefined_languages[language]}</Link>
    //       </li>
    //     ))}
    //   </ul>
    // </li>
    <div>
      <h2 className='text-white h5 mt-3'><Trans>Languages</Trans></h2>
      <div className='d-flex' style={{ gap: '10px' }}>
        {languages.map((lang) => (
          <Link style={{ cursor: 'pointer' }} key={lang} onClick={(e) => {
            e.preventDefault();
            changeLanguage(lang);
          }} className={`btn btn-${language === lang ? 'primary' : 'secondary'}`} to={'/'}>{predefined_languages[lang]}</Link>
        ))}
      </div>
    </div>
  );
};

export default LanguageSwitcher;