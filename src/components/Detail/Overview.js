import React from 'react'
import { isMobile } from 'react-device-detect';
import { Trans } from 'react-i18next'

const Element = ({ properties }) => {

    let overview = properties.html
    properties = properties.frontmatter
    
    return (
        <div className={`container-fluid ${isMobile ? 'px-2' : 'px-3'}`}>
            <div className={`bg-secondary ${isMobile ? 'p-4' : 'p-5'}`}>
                <div className='d-flex align-items-center justify-content-between flex-wrap' style={{ gap: '10px' }}>
                    <h3 className='h2 mb-0 text-white'><Trans>Overview</Trans></h3>
                    <h3 className='h6 mb-0 text-white fw-normal'>{properties.date}</h3>
                </div>
                <div className='w-100 bg-tertiary my-3' style={{ height: '2px' }}></div>
                {/* <div className='text-white' >
                    {properties.overview}
                </div> */}
                <section className='text-white py-3'
                    dangerouslySetInnerHTML={{ __html: overview }}
                    itemProp="articleBody"
                />
                <div className={`d-flex align-items-center mt-4 flex-wrap ${isMobile ? 'justify-content-between' : ''}`} style={{ gap: '20px 8em' }}>
                    <div className={isMobile ? 'flex-fill' : ''}>
                        <h4 className='text-white mb-0'>{properties.housing_type}</h4>
                        <h4 className='h6 text-secondary mb-0'><Trans>Property Type</Trans></h4>
                    </div>
                    <div className={isMobile ? 'flex-fill' : ''}>
                        <h4 className='text-white mb-0 text-end'>{properties.bedrooms}</h4>
                        <h4 className='h6 text-secondary mb-0'><Trans>Bedrooms</Trans></h4>
                    </div>
                    <div className={isMobile ? 'flex-fill' : ''}>
                        <h4 className='text-white mb-0 text-end'>{properties.bathrooms}</h4>
                        <h4 className='h6 text-secondary mb-0 text-end'><Trans>Bathrooms</Trans></h4>
                    </div>
                    <div className={isMobile ? 'flex-fill' : ''}>
                        <h4 className='text-white mb-0 text-end'>{properties.car_port}</h4>
                        <h4 className='h6 text-secondary mb-0 text-end'><Trans>Car Port</Trans></h4>
                    </div>
                    <div className={isMobile ? 'flex-fill' : ''}>
                        <h4 className='text-white mb-0 text-end'>{properties.total_area} m²</h4>
                        <h4 className='h6 text-secondary mb-0 text-end'><Trans>Total Area</Trans></h4>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Element