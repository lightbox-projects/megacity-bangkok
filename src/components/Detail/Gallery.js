import React, { useState } from 'react'
// import { StaticImage } from "gatsby-plugin-image"
import { isMobile } from 'react-device-detect';
import { Trans, useTranslation } from 'react-i18next'

const Element = ({ properties }) => {
    const [currentImage, setCurrentImage] = useState(null)
    const [activeIdx, setActiveIdx] = useState(null)
    const { t } = useTranslation();

    properties = properties.frontmatter

    const [state, setState] = React.useState({
        name: '',
        email: '',
        phone: '',
        day: '',
        time: '',
    });

    function handleClick(url, idx) {
        setCurrentImage(url)
        setActiveIdx(idx)

        setTimeout(() => {
            setActiveIdx(null)
            setCurrentImage(null)
        }, 10000)
    }

    const handleChange = (event, id) => {
        setState({ ...state, [id]: event.target.value });
    };

    function handleTour() {
        let subject = `Booking a tour to see ${properties.title} property on ${state.day} ${state.time}.`
        let body = `
            Hello im ${state.name}, Im looking to see the property on ${state.day} ${state.time}. This is my contact for confirmation, Email : ${state.email} Phone : ${state.phone}. Im looking forward to see the property, thank you.
        `
        window.open(`mailto:thawdarkhant@gmail.com?subject=${subject}&body=${body}`)
    }

    return (
        <div className='container-fluid px-3 mb-4'>

            <div className={`row ${isMobile ? 'flex-nowrap pb-2 mx-1' : ''}`} style={{ gap: '10px 0', overflowX: isMobile ? 'scroll' : '' }}>
                <div className={`col-lg-8 ${isMobile ? 'px-0' : ''}`}>
                    {
                        currentImage != null ?
                            <img src={currentImage}
                                style={{ aspectRatio: '1/1', objectFit: 'cover', minWidth: '150px', maxHeight: '69vh' }} className='w-100 h-100' alt='an image' />
                            :
                            <img src={properties.banner_image}
                                style={{ aspectRatio: '1/1', objectFit: 'cover', minWidth: '150px', maxHeight: '69vh' }} className='w-100 h-100' alt='an image' />
                    }
                </div>
                <div className={`col-lg-4 ${isMobile ? 'px-0' : ''}`}>
                    <div className='h-100 d-flex flex-column bg-secondary p-4'>
                        <div className='my-auto text-white'>
                            <div className='text-center h3 fw-bolder mb-5'>
                                <Trans>Book a Tour</Trans>
                            </div>
                            <div className='d-flex flex-column' style={{ gap: '15px' }}>
                                <div>
                                    <input className='form-control' value={state.name} onChange={(event) => handleChange(event, 'name')} placeholder={t('Name')} />
                                </div>
                                <div>
                                    <input className='form-control' type='email' value={state.email} onChange={(event) => handleChange(event, 'email')} placeholder={t('Email')} />
                                </div>
                                <div>
                                    <input className='form-control' type='number' value={state.phone} onChange={(event) => handleChange(event, 'phone')} placeholder={t('Phone Number')} />
                                </div>
                                <div className='d-flex align-items-center flex-wrap' style={{ gap: '15px' }}>
                                    <div className='flex-fill'>
                                        <input className='form-control' type='date' value={state.day} onChange={(event) => handleChange(event, 'day')} />
                                    </div>
                                    <div className='flex-fill'>
                                        <input className='form-control' type='time' value={state.time} onChange={(event) => handleChange(event, 'time')} />
                                    </div>
                                </div>
                            </div>
                            <div className='w-100 d-flex mt-4'>
                                <button className='btn btn-warning m-auto px-5' onClick={() => handleTour()}>
                                    <Trans>Schedule a Tour</Trans>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            {properties.thumbnails && properties.thumbnails.length > 0 ?
                <div className='d-flex pb-2 mt-4' style={{ overflowX: 'auto', gap: '10px', width: 'calc(100vw-200px)' }}>
                    {properties.thumbnails.map((item, idx) => (
                        <img key={idx + 'IMAGEKEY'} src={item} onClick={() => handleClick(item, idx)}
                            style={{ aspectRatio: '1/1', objectFit: 'cover', minWidth: '150px', maxHeight: '200px' }} className={`w-100 detail-image-show ${activeIdx != null ? (activeIdx === idx ? '' : 'not-selected') : ''}`} alt={item} />
                    ))}
                </div>
                : null
            }
        </div>
    );
}

export default Element