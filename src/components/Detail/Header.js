import React from 'react'
import { isMobile } from 'react-device-detect';
import { Trans } from 'react-i18next'

const Header = ({ properties }) => {

    properties = properties.frontmatter

    function currency(nominal) {
        let BathCurrency = new Intl.NumberFormat('th', {
            style: 'currency',
            currency: 'THB',
        });

        return BathCurrency.format(nominal)
    }

    return (
        <div className='container-fluid px-5 pb-4 mb-2 pt-4'>
            <div className='d-flex align-items-center flex-wrap justify-content-between' style={{ gap: '10px 30px' }}>
                <div style={{ maxWidth: '1200px' }}>
                    <h4 className='h1'>{properties.title}</h4>
                    <div style={{ gap: '10px' }} className='d-flex align-items-center'>
                        {
                            isMobile ? null :
                                <i className='bx bx-map'></i>
                        }
                        {properties.address}
                    </div>
                </div>
                <div className={`text- ${isMobile ? 'w-100 d-flex align-items-center justify-content-between my-3' : ''}`}>
                    {
                        isMobile ?
                            <div className='h5 fw-bolder mb-0'>
                                <Trans>Total</Trans> <br /> <Trans>Area</Trans>
                            </div>
                            :
                            <div>
                            </div>
                    }
                    <div>
                        <h4 className='h1 text-end mb-0'>{currency(properties.price)}</h4>
                        <div style={{ gap: '10px' }} className='d-flex align-items-center justify-content-end'>
                            <i className='ti ti-ruler'></i>
                            {properties.total_area} m²
                        </div>
                    </div>
                </div>
            </div>

            <div className='d-flex align-items-center mt-4' style={{ gap: '10px', maxWidth: '400px' }}>
                {properties.is_featured ?
                    <button className='btn btn-success flex-fill'>
                        <Trans>Featured</Trans>
                    </button>
                    :
                    null
                }
                {properties.status.includes('Sale') ?
                    <button className='btn btn-primary flex-fill'>
                        <Trans>For Sale</Trans>
                    </button>
                    : null
                }
                {properties.status.includes('Rent') ?
                    <button style={{ background: '#8C801A' }} className='btn flex-fill text-white'>
                        <Trans>For Rent</Trans>
                    </button>
                    : null
                }
            </div>

        </div>
    );
}

export default Header