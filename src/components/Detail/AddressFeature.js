import React from 'react'
import { isMobile } from 'react-device-detect';
import { Trans } from 'react-i18next'

const Element = ({ properties }) => {

    properties = properties.frontmatter
    let map_location = JSON.parse(properties.map_location)

    return (
        <div className={`container-fluid ${isMobile ? 'px-2 my-2' : 'px-3 my-4'}`} style={{ overflowX: 'hidden' }}>
            <div className='row ' style={{ gap: '10px 0' }}>
                <div className='col-lg-6 '>
                    <div className={`bg-secondary w-100 h-100 ${isMobile ? 'p-4' : 'p-5'}`}>
                        <div className='d-flex align-items-center justify-content-between flex-wrap' style={{ gap: '10px' }}>
                            <h2 className='text-white mb-0'><Trans>Address</Trans></h2>
                            <a target='_blank' href={`https://maps.google.com/?z=15&q=${map_location.coordinates[1]},${map_location.coordinates[0]}`} className='btn btn-warning'>
                                <Trans>View on Google Map</Trans>
                            </a>
                        </div>

                        <table className='text-white mt-4'>
                            <tbody>
                                <tr>
                                    <td style={{ width: '150px' }}><Trans>Address</Trans></td>
                                    <td>{properties.address}</td>
                                </tr>
                                <tr>
                                    <td className='py-2'><Trans>State</Trans></td>
                                    <td>{properties.state}</td>
                                </tr>
                                {/* <tr>
                                    <td>State/Country</td>
                                    <td>Nonthaburi</td>
                                </tr> */}
                                <tr>
                                    <td className='pt-2'><Trans>Zip Code</Trans></td>
                                    <td>{properties.zip_code}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div className='col-lg-6 '>
                    <div className={`bg-secondary w-100 h-100 ${isMobile ? 'p-4' : 'p-5'}`}>
                        <div className='d-flex align-items-center justify-content-between flex-wrap' style={{ gap: '10px' }}>
                            <h2 className='text-white mb-0'><Trans>Features</Trans></h2>
                            {/* <h2 className='text-white mb-0'>Powered By Yelp</h2> */}
                        </div>

                        {properties.features && properties.features.length > 0 ?
                            <ul className='text-white mt-4 px-3'>
                                {properties.features.map(item => (
                                    <li key={item}>{item}</li>
                                ))}
                            </ul>
                            : null}
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Element