import React from 'react'
import { StaticImage } from "gatsby-plugin-image"
// import { Swiper, SwiperSlide } from 'swiper/react';
import { Trans } from 'react-i18next'

const Section3 = () => {
    return (
        <div className='position-relative'>
            <StaticImage src='../../../static/assets/cities-aerial.jpg' alt='Find your place with' className='position-absolute h-100 w-100' style={{ zIndex: -1, top: 0, left: 0, objectFit: 'cover', }} />
            <div style={{ background: 'black', opacity: .7, top: 0, zIndex: -1, }} className='w-100 h-100 position-absolute' />

            <div className='container-lg py-5' style={{ zIndex: 99 }}>
                <div className='py-5 px-2'>
                    <h1 className='fw-bolder display-3 text-white'><Trans> Find Your Place With </Trans></h1>
                    <h6 className='h5 fw-normal text-white'>megacitybangkok.com</h6>
                </div>

                <div className='row' style={{ gap: '10px 0' }}>
                    <div className='col-lg-4'>
                        <div className='d-flex align-items-center justify-content-center' style={{ gap: '10px 20px' }}>
                            <h1 className='mb-0 text-white fw-bolder'>01</h1>
                            <h1 className='h3 mb-0 text-white'><Trans>Easy To Use</Trans></h1>
                        </div>
                        <p className='text-white text-center mt-3'>
                            <Trans>by using our filtered database to find your property needs be it renting or buying houses, apartement, condo, and etc.</Trans>
                        </p>
                    </div>
                    <div className='col-lg-4'>
                        <div className='d-flex align-items-center justify-content-center' style={{ gap: '10px 20px' }}>
                            <h1 className='mb-0 text-white fw-bolder'>02</h1>
                            <h1 className='h3 mb-0 text-white'><Trans>Trusted Properties Finders</Trans></h1>
                        </div>
                        <p className='text-white text-center mt-3'>
                            <Trans>We always put our clients trust in everything and our clients satisfaction is top priority</Trans>
                        </p>
                    </div>
                    <div className='col-lg-4'>
                        <div className='d-flex align-items-center justify-content-center' style={{ gap: '10px 20px' }}>
                            <h1 className='mb-0 text-white fw-bolder'>03</h1>
                            <h1 className='h3 mb-0 text-white'><Trans>All Services</Trans></h1>
                        </div>
                        <p className='text-white text-center mt-3'>
                            <Trans>From visitation to consultation we provide a wide range of services such as consultation of property investment, legalization and ownership transfering</Trans>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Section3