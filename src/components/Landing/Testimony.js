import React from 'react'
import { StaticImage } from "gatsby-plugin-image"
import { Swiper, SwiperSlide } from 'swiper/react';
import { Autoplay } from 'swiper/modules';
import { isMobile } from 'react-device-detect';
import { Trans } from 'react-i18next'

const Testimony = ({ testimony }) => { 

    return (
        <div className='position-relative'>
            <StaticImage src='../../../static/assets/cities-aerial.jpg' alt='BACKGROUND TESTIMONY' className='position-absolute h-100 w-100' style={{ zIndex: -1, top: 0, left: 0, objectFit: 'cover', }} />
            <div style={{ background: 'black', opacity: .7, top: 0, zIndex: -1, }} className='w-100 h-100 position-absolute' />

            <div className={`container-lg py-5 ${isMobile ? 'px-4' : ''}`} style={{ zIndex: 99 }}>
                <div className='py-5'>
                    <h1 className='fw-bolder display-3 text-white'><Trans>Testimonials</Trans></h1>
                    <h6 className='h5 fw-normal text-white'>megacitybangkok.com</h6>
                </div>

                <Swiper className='mt-5'
                    // install Swiper modules
                    modules={[Autoplay]}
                    autoplay={{
                        delay: 2500,
                        disableOnInteraction: false,
                    }}
                    spaceBetween={isMobile ? 'auto' : 30}
                    slidesPerView={'auto'}
                    height={100}
                >
                    {
                        testimony.map((item, idx) => (
                            <SwiperSlide key={item.name + "" + idx + 'TESTIMONY'} className='p-5 d-flex a flex-column' style={{ height: 'auto', maxWidth: isMobile ? '100%' : '350px', background: '#FFDA93', borderRadius: '10px', border: '1px solid var(--tertiary)' }}>
                                <div className='d-flex mb-3'>
                                    <img src={item.frontmatter.thumbnail} alt={item + 'TESTIMONIAL'}
                                        className='rounded-circle m-auto' style={{ float: 'center', height: '150px', width: '150px', objectFit: 'cover', }} />
                                </div>
                                <div className='py-3' style={{ fontSize: '13px', textAlign: 'justify' }} dangerouslySetInnerHTML={{ __html: item.html }}>
                                </div>
                                <h4 className='h6 mt-3 mb-0 fw-bolder text-center mt-auto'>{item.frontmatter.name}</h4>
                            </SwiperSlide>
                        ))
                    }
                </Swiper>

            </div>
        </div>
    );
}

export default Testimony