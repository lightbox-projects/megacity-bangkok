import React from 'react'
import { StaticImage } from "gatsby-plugin-image"
import { isMobile } from 'react-device-detect';
import { Trans } from 'react-i18next'

const Jumbotron = () => {
    return (
        <div className='position-relative'>
            {isMobile ?
                <StaticImage src='../../../static/assets/cities-aerial.jpg' alt='Jumbotron' className='w-100' style={{ aspectRatio: 'unset', height: '70vh', objectFit: 'cover', }} />
                :
                <StaticImage src='../../../static/assets/cities-aerial.jpg' alt='Jumbotron' className='w-100' style={{ aspectRatio: 'unset', maxHeight: '80vh', objectFit: 'cover', }} />
            }
            <div style={{ background: 'black', opacity: .45, top: 0 }} className='w-100 h-100 position-absolute' />
            {/* <div style={{ background: 'white', opacity: .15, top: 0 }} className='w-100 h-100 position-absolute' /> */}
            <div className='w-100 h-100 position-absolute d-flex flex-column align-items-center justify-content-center px-3' style={{ zIndex: 10, top: 0 }}>
                <StaticImage src='../../../static/assets/logo.png' alt='Jumbotron Logo' className='w-100' style={{ maxWidth: isMobile ? '150px' : '300px' }} />
                <h1 className='display-3 text-center fw-bolder text-white'><Trans>Your Best Property Companion</Trans></h1>
                <h1 className='text-center fw-normal h4 text-white'>
                    <Trans>The new ways of finding your home.</Trans>
                </h1>
            </div>
        </div>
    );
}

export default Jumbotron