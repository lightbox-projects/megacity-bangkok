import React from 'react'
import { Swiper, SwiperSlide } from 'swiper/react';
import { Pagination } from 'swiper/modules';
import { isMobile } from 'react-device-detect';
import { Trans } from 'react-i18next'

import PropertyCard from './PropertyCard'

const ItemSwiper = ({ properties }) => {

    return (
        <div className='bg-secondary'>
            <div className='container-lg py-5'>
                <h1 className='display-4 text-center fw-bolder text-white'><Trans>Popular Properties</Trans></h1>
                <h1 className='text-center fw-normal h6 text-white'>
                    <Trans>lighting layout, and showing portfolio.</Trans>
                </h1>

                <div className='mt-5'>
                    {isMobile ?
                        <Swiper
                            // install Swiper modules
                            modules={[Pagination]}
                            spaceBetween={30}
                            slidesPerView={'auto'}
                            pagination={{ clickable: true }}
                        >
                            {
                                properties.map(item => (
                                    <SwiperSlide key={'FEATURED_CARD' + item.fields.slug} className='h-100' style={{ maxWidth: '100%' }}>
                                        <PropertyCard properties={item.frontmatter} slug={item.fields.slug} />
                                    </SwiperSlide>
                                ))
                            }
                        </Swiper>
                        :
                        <div className='row' style={{ gap: '10px 0' }}>
                            {properties.map(item => (
                                <div key={'FEATURED_CARD' + item.fields.slug} className='col-lg-3'>
                                    <PropertyCard properties={item.frontmatter} slug={item.fields.slug} />
                                </div>
                            ))}
                        </div>
                    }
                </div>

            </div>
        </div>
    );
}

export default ItemSwiper