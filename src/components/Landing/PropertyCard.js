import React from 'react'
// import { StaticImage } from "gatsby-plugin-image"
import { navigate } from 'gatsby'
import { Trans } from 'react-i18next'

const Component = ({ minimize, properties, slug }) => {

    function onRoute() {
        navigate('/detail/' + slug.replace('/property/', ''));
    }

    return (
        minimize ?
            <div className='bg-white h-100 w-100 d-flex flex-column' style={{ border: '1px solid var(--tertiary)', cursor: 'pointer' }} onClick={onRoute}>
                <div className='position-relative'>
                    <img style={{ aspectRatio: '1/1', objectFit: 'cover' }} className='w-100' src={properties.banner_image} alt={properties.title} />
                    {properties.is_featured ?
                        <button className='btn btn-sm btn-success position-absolute' style={{ top: '10px', left: '10px', fontSize: '10px' }}>
                            <Trans>Featured</Trans>
                        </button>
                        :
                        null
                    }
                    <div className='position-absolute d-flex flex-column' style={{ top: '10px', right: '10px', gap: '5px' }}>
                        {properties.status.includes('Sale') ?
                            <button className='btn btn-sm btn-primary' style={{ fontSize: '10px' }}>
                                <Trans>For Sale</Trans>
                            </button>
                            : null
                        }
                        {properties.status.includes('Rent') ?
                            <button className='btn btn-sm text-white' style={{ fontSize: '10px', background: '#8C801A' }}>
                                <Trans>For Rent</Trans>
                            </button>
                            : null
                        }
                    </div>
                </div>
                <div className='p-2 pb-0'>
                    <div className='mb-0 fw-bolder' style={{ fontSize: '12px' }}>{properties.title}</div>
                </div>
                <div className='p-2 pt-0 mt-auto'>
                    <div className='mb-0 fw-bolder text-secondary' style={{ fontSize: '12px' }}>{properties.state}</div>

                    <hr className='my-1' />

                    <div className='d-flex align-items-center' style={{ gap: '5px' }}>
                        <div className='d-flex align-items-center' style={{ gap: '2px', }}>
                            <i className='ti ti-bed' style={{ fontSize: '14px' }}></i>
                            <span className='fw-bold' style={{ fontSize: '12px' }}>{properties.bedrooms}</span>
                        </div>
                        <div className='d-flex align-items-center' style={{ gap: '2px', }}>
                            <i className='ti ti-bath' style={{ fontSize: '14px' }}></i>
                            <span className='fw-bold' style={{ fontSize: '12px' }}>{properties.bathrooms}</span>
                        </div>
                        <div className='d-flex align-items-center' style={{ gap: '2px', }}>
                            <i className='ti ti-car' style={{ fontSize: '14px' }}></i>
                            <span className='fw-bold' style={{ fontSize: '12px' }}>{properties.car_port}</span>
                        </div>
                        <div className='d-flex align-items-center' style={{ gap: '2px', }}>
                            <i className='ti ti-ruler-2' style={{ fontSize: '14px' }}></i>
                            <span className='fw-bold' style={{ fontSize: '12px' }}>{properties.total_area}m<sup>2</sup></span>
                        </div>
                    </div>
                </div>
            </div>
            :
            <div className='bg-white h-100 w-100 d-flex flex-column' style={{ border: '1px solid var(--tertiary)', cursor: 'pointer' }} onClick={onRoute}>
                <div className='position-relative'>
                    <img style={{ aspectRatio: '1/1', objectFit: 'cover' }} className='w-100' src={properties.banner_image} alt={properties.title} />
                    {properties.is_featured ?
                        <button className='btn btn-sm px-4 btn-success position-absolute' style={{ top: '10px', left: '10px' }}>
                            <Trans>Featured</Trans>
                        </button>
                        :
                        null
                    }
                    <div className='position-absolute d-flex flex-column' style={{ top: '10px', right: '10px', gap: '5px' }}>
                        {properties.status.includes('Sale') ?
                            <button className='btn btn-sm px-4 btn-primary'>
                                <Trans>For Sale</Trans>
                            </button>
                            : null
                        }
                        {properties.status.includes('Rent') ?
                            <button className='btn btn-sm px-4 text-white' style={{ background: '#8C801A' }}>
                                <Trans>For Rent</Trans>
                            </button>
                            : null
                        }
                    </div>
                </div>
                <div className='p-3 pb-0'>
                    <h4 className='mb-0 fw-bolder h6'>{properties.title}</h4>
                    <div style={{ fontSize: '12px' }}>{properties.address}</div>
                </div>
                <div className='p-3 pt-0 mt-auto'>

                    <div className='d-flex align-items-center my-2 mt-auto' style={{ gap: '20px' }}>
                        <div className='d-flex align-items-center' style={{ gap: '8px', }}>
                            <i className='ti ti-bed' style={{ fontSize: '22px' }}></i>
                            <span className='fw-bold' style={{ fontSize: '14px' }}>{properties.bedrooms}</span>
                        </div>
                        <div className='d-flex align-items-center' style={{ gap: '8px', }}>
                            <i className='ti ti-bath' style={{ fontSize: '22px' }}></i>
                            <span className='fw-bold' style={{ fontSize: '14px' }}>{properties.bathrooms}</span>
                        </div>
                        <div className='d-flex align-items-center' style={{ gap: '8px', }}>
                            <i className='ti ti-car' style={{ fontSize: '22px' }}></i>
                            <span className='fw-bold' style={{ fontSize: '14px' }}>{properties.car_port}</span>
                        </div>
                        <div className='d-flex align-items-center' style={{ gap: '8px', }}>
                            <i className='ti ti-ruler-2' style={{ fontSize: '22px' }}></i>
                            <span className='fw-bold' style={{ fontSize: '14px' }}>{properties.total_area} m<sup>2</sup></span>
                        </div>
                    </div>

                    <h4 className='mb-0 fw-bolder text-secondary h5'>{properties.housing_type}</h4>
                    <div style={{ background: 'var(--tertiary)', height: '2px', }} className='w-100 my-2' />
                    <div className='d-flex flex-wrap align-items-center justify-content-between' style={{ gap: '10px' }}>
                        <h4 className='mb-0 text-secondary mb-0' style={{ fontSize: '12px' }}><Trans>Posted By Administrator</Trans></h4>
                        <h4 className='mb-0 text-secondary mb-0' style={{ fontSize: '12px' }}>{properties.date}</h4>
                    </div>
                </div>
            </div>
    );
}

export default Component