import React, { useEffect } from 'react'
import { isMobile } from 'react-device-detect';
import { navigate } from 'gatsby'
import { Trans } from 'react-i18next'
import { useI18next } from 'gatsby-plugin-react-i18next';

const SearchFunction = ({ currentState }) => {
    const [state, setState] = React.useState({
        housing_type: '',
        housing_sub_type: '',
        state: '',
        property_size: '',
        budget: '',
        status: '',
    });
    const { language } = useI18next();

    const states = ['Amnatcharoen', 'Angthong', 'Ayutthaya', 'Bangkok', 'Betong', 'Buriram', 'Chachoengsao', 'Chainat', 'Chaiyaphum', 'Chanthaburi', 'Chiangmai', 'Chiangrai', 'Chonburi', 'Chumphon', 'Kalasin', 'Kamphaengphet', 'Kanchanaburi', 'Khonkaen', 'Krabi', 'Lampang', 'Lamphun', 'Loei', 'Lopburi', 'Maehongson', 'Mahasarakham', 'Mukdahan', 'Nakhonnayok', 'Nakhonpathom', 'Nakhonphanom', 'Nakhonratchasima', 'Nakhonsawan', 'Nakhonsithammarat', 'Nan', 'Narathiwat', 'Nongbualamphu', 'Nongkhai', 'Nonthaburi', 'Pathumthani', 'Pattani', 'Phangnga', 'Phatthalung', 'Phayao', 'Phetchabun', 'Phetchaburi', 'Phichit', 'Phitsanulok', 'Phrae', 'Phuket', 'Prachinburi', 'Prachuapkhirikhan', 'Ranong', 'Ratchaburi', 'Rayong', 'Roiet', 'Sakaeo', 'Sakonnakhon', 'Samutprakan', 'Samutsakhon', 'Samutsongkhram', 'Saraburi', 'Satun', 'Singburi', 'Sisaket', 'Songkhla', 'Sukhothai', 'Suphanburi', 'Suratthani', 'Surin', 'Tak', 'Trang', 'Trat', 'Ubonratchathani', 'Udonthani', 'Uthaithani', 'Uttaradit', 'Yala', 'Yasothon']

    const handleChange = (event, id) => {
        setState({ ...state, [id]: event.target.value });
    };

    function handleSubmit() {
        if (language != 'en')
            navigate('/' + language + '/listing#search', {
                state: { ...state },
            })
        else
            navigate('/listing#search', {
                state: { ...state },
            })
    }

    useEffect(() => {
        setState({ ...currentState })
    }, [])

    return (
        <div className='bg-secondary pt-5 search-function'>
            <div className='container d-flex flex-column align-items-center w-100 position-relative' style={{ marginTop: isMobile ? '-30%' : '0', zIndex: 10 }}>
                {/* <div className='d-flex mx-auto' style={{ overflowX: 'auto', maxWidth: '100vw' }}>
                    <button className='btn btn-tertiary text-white p-3' style={{ borderRadius: '10px 0 0 0', minWidth: '150px' }}>
                        All Properties
                    </button>
                    <button className='btn btn-outline-warning bg-white text-dark p-3' style={{ borderRadius: '0 0 0 0', minWidth: '150px' }}>
                        For Rent
                    </button>
                    <button className='btn btn-outline-warning bg-white text-dark p-3' style={{ borderRadius: '0 10px 0 0', minWidth: '150px' }}>
                        For Sale
                    </button>
                </div> */}
                {/* {JSON.stringify(state)} */}
                <div className='w-100 py-3 px-4 bg-white' style={{ borderRadius: '5px' }}>
                    <div className='row align-items-end' style={{ gap: '10px' }}>
                        <div className='col-lg' style={{ minWidth: '200px' }}>
                            <label htmlFor="looking_for" className="h6 fw-bold form-label"><Trans>Looking For</Trans></label>
                            <div className='d-flex ' style={{ gap: '10px' }}>
                                <select className='form-control' value={state.housing_type} onChange={(event) => handleChange(event, 'housing_type')} style={{ height: '30px' }} placeholder='Property Type' id='looking_for'>
                                    <option value={''}><Trans>All Type</Trans></option>
                                    <option value={'Apartement'}><Trans>Apartement</Trans></option>
                                    <option value={'House'}><Trans>House</Trans></option>
                                </select>
                                <select className='form-control' value={state.housing_sub_type} onChange={(event) => handleChange(event, 'housing_sub_type')} style={{ height: '30px' }} placeholder='Property Type' id='looking_for'>
                                    <option value={''}><Trans>All Sub Type</Trans></option>
                                    <option value={'Duplex'}><Trans>DUPLEX</Trans></option>
                                    <option value={'Convertible'}><Trans>CONVERTIBLE</Trans></option>
                                    <option value={'Studio'}><Trans>STUDIO</Trans></option>
                                    <option value={'Triplex'}><Trans>TRIPLEX</Trans></option>
                                    <option value={'Single-Family'}><Trans>Single-Family</Trans></option>
                                    <option value={'Bungalow'}><Trans>Bungalow</Trans></option>
                                    <option value={'Condominium'}><Trans>Condominium</Trans></option>
                                    <option value={'Cottage'}><Trans>Cottage</Trans></option>
                                </select>
                            </div>
                        </div>
                        <div className='col-lg' style={{ minWidth: '200px' }}>
                            <label htmlFor="looking_for" className="h6 fw-bold form-label"><Trans>Location</Trans></label>
                            <select className='form-control' value={state.state} onChange={(event) => handleChange(event, 'state')} placeholder='Property Type' id='looking_for'>
                                <option value={''}><Trans>All Location</Trans></option>
                                {states.map(item => (
                                    <option key={'LOCATION-' + item} value={item}>{item}</option>
                                ))}
                            </select>
                        </div>
                        <div className='col-lg' style={{ minWidth: '200px' }}>
                            <label htmlFor="looking_for" className="h6 fw-bold form-label"><Trans>Property Sizes</Trans></label>
                            <select className='form-control' value={state.property_size} onChange={(event) => handleChange(event, 'property_size')} placeholder='Property Type' id='looking_for'>
                                <option value={''}><Trans>All Size</Trans></option>
                                <option value={'Small'}><Trans>Small</Trans></option>
                                <option value={'Medium'}><Trans>Medium</Trans></option>
                                <option value={'Large'}><Trans>Large</Trans></option>
                            </select>
                        </div>
                        <div className='col-lg' style={{ minWidth: '200px' }}>
                            <label htmlFor="looking_for" className="h6 fw-bold form-label"><Trans>Your Budget</Trans></label>
                            <select className='form-control' value={state.budget} onChange={(event) => handleChange(event, 'budget')} placeholder='Property Type' id='looking_for'>
                                <option value={''}><Trans>All Budget</Trans></option>
                                <option value={[100000, 300000]}>100.000 - 300.000</option>
                                <option value={[300000, 600000]}>300.000 - 600.000</option>
                                <option value={[600000, 900000]}>600.000 - 900.000</option>
                            </select>
                        </div>
                        <div className='col-lg' style={{ minWidth: '200px' }}>
                            <label htmlFor="looking_for" className="h6 fw-bold form-label"><Trans>Status</Trans></label>
                            <select className='form-control' value={state.status} onChange={(event) => handleChange(event, 'status')} placeholder='Property Type' id='looking_for'>
                                <option value={''}><Trans>All Status</Trans></option>
                                <option value={'Rent'}><Trans>Rent</Trans></option>
                                <option value={'Sale'}><Trans>Sale</Trans></option>
                            </select>
                        </div>
                        <div className='col-lg' style={{ minWidth: '200px' }}>
                            {/* <label htmlFor="looking_for" className="h6 fw-bold form-label" style={{ opacity: 0 }}>Looking For</label> */}
                            <button className='btn btn-secondary w-100' onClick={() => handleSubmit()} style={{ height: '40px' }}><i className='ti ti-search'></i> <Trans>Search</Trans></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default SearchFunction