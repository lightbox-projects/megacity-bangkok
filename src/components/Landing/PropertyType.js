import React, { useState } from 'react'
import { StaticImage } from "gatsby-plugin-image"
import { Trans } from 'react-i18next'
import { navigate } from 'gatsby'
import { useI18next } from 'gatsby-plugin-react-i18next';

const PropertyType = () => {

    const [currCategory, setCurrCategory] = useState('Apartement')
    const { language } = useI18next();

    function handleCategory(type) {
        setCurrCategory(type)
    }

    function handleRoute(val) {
        if (language != 'en')
            navigate('/' + language + '/listing#search', {
                state: { housing_sub_type: val },
            })
        else
            navigate('/listing#search', {
                state: { housing_sub_type: val },
            })

    }

    return (
        <div className='container-lg py-5 my-5'>
            <div className='row' style={{ gap: '10px' }}>
                <div className='col-lg'>
                    <div>
                        <div className='text-center' onMouseOver={() => handleCategory('Apartement')}>
                            <h1 className='fw-bolder'><Trans>Apartements</Trans></h1>
                            <p><Trans>Elevate your lifestyle – seize the opportunity to own a sophisticated apartment that embodies comfort and style.</Trans></p>
                        </div>
                        {currCategory === 'Apartement' &&
                            <div className='' style={{ width: '150px', height: '2px', background: 'var(--tertiary)', float: 'right ' }} />
                        }
                    </div>
                    <div className='mt-5'>
                        <div className='text-center' onMouseOver={() => handleCategory('House')}>
                            <h1 className='fw-bolder'><Trans>Houses</Trans></h1>
                            <p><Trans>a place where every detail is designed for your well-being and joy. Welcome home to a life of tranquility and beauty.</Trans></p>
                        </div>
                        {currCategory === 'House' &&
                            <div className='' style={{ width: '150px', height: '2px', background: 'var(--tertiary)', float: 'right ' }} />
                        }
                    </div>
                </div>
                {currCategory === 'Apartement' &&
                    <div className='col-lg'>
                        <div className='position-relative' onClick={() => handleRoute('Studio')} style={{ cursor: 'pointer' }}>
                            <StaticImage style={{ aspectRatio: '1/1', objectFit: 'cover' }} src="../../../static/assets/Rectangle 714.jpg" alt="studio type" />
                            <div className='position-absolute w-100 h-100' style={{ top: 0, left: 0, background: 'var(--tertiary)', opacity: .4 }} />
                            <div className='position-absolute' style={{ bottom: '10px', left: '20px' }}>
                                <h2 className='fw-bolder'><Trans>STUDIO</Trans></h2>
                            </div>
                        </div>
                        <div className='position-relative mt-4' onClick={() => handleRoute('Duplex')} style={{ cursor: 'pointer' }}>
                            <StaticImage style={{ aspectRatio: '1/1', objectFit: 'cover' }} src="../../../static/assets/Rectangle 716.jpg" alt="duplex type" />
                            <div className='position-absolute w-100 h-100' style={{ top: 0, left: 0, background: 'var(--tertiary)', opacity: .4 }} />
                            <div className='position-absolute' style={{ bottom: '10px', left: '20px' }}>
                                <h2 className='fw-bolder'><Trans>DUPLEX</Trans></h2>
                            </div>
                        </div>
                    </div>
                }
                {currCategory === 'Apartement' &&
                    <div className='col-lg'>
                        <div className='position-relative' onClick={() => handleRoute('Convertible')} style={{ cursor: 'pointer' }}>
                            <StaticImage style={{ aspectRatio: '1/1', objectFit: 'cover' }} src="../../../static/assets/Rectangle 697.jpg" alt="convertible type" />
                            <div className='position-absolute w-100 h-100' style={{ top: 0, left: 0, background: 'var(--tertiary)', opacity: .4 }} />
                            <div className='position-absolute' style={{ bottom: '10px', left: '20px' }}>
                                <h2 className='fw-bolder'><Trans>CONVERTIBLE</Trans></h2>
                            </div>
                        </div>
                        <div className='position-relative mt-4' onClick={() => handleRoute('Triplex')} style={{ cursor: 'pointer' }}>
                            <StaticImage style={{ aspectRatio: '1/1', objectFit: 'cover' }} src="../../../static/assets/Rectangle 715.jpg" alt="triplex type" />
                            <div className='position-absolute w-100 h-100' style={{ top: 0, left: 0, background: 'var(--tertiary)', opacity: .4 }} />
                            <div className='position-absolute' style={{ bottom: '10px', left: '20px' }}>
                                <h2 className='fw-bolder'><Trans>TRIPLEX</Trans></h2>
                            </div>
                        </div>
                    </div>
                }
                {currCategory === 'House' &&
                    <div className='col-lg'>
                        <div className='position-relative' onClick={() => handleRoute('Single-Family')} style={{ cursor: 'pointer' }}>
                            <StaticImage style={{ aspectRatio: '1/1', objectFit: 'cover' }} src="../../../static/assets/asset-landing-3.jpeg" alt="studio type" />
                            <div className='position-absolute w-100 h-100' style={{ top: 0, left: 0, background: 'var(--tertiary)', opacity: .4 }} />
                            <div className='position-absolute' style={{ bottom: '10px', left: '20px' }}>
                                <h2 className='fw-bolder'><Trans>Single-Family</Trans></h2>
                            </div>
                        </div>
                        <div className='position-relative mt-4' onClick={() => handleRoute('Bungalow')} style={{ cursor: 'pointer' }}>
                            <StaticImage style={{ aspectRatio: '1/1', objectFit: 'cover' }} src="../../../static/assets/asset-landing-3.jpeg" alt="duplex type" />
                            <div className='position-absolute w-100 h-100' style={{ top: 0, left: 0, background: 'var(--tertiary)', opacity: .4 }} />
                            <div className='position-absolute' style={{ bottom: '10px', left: '20px' }}>
                                <h2 className='fw-bolder'><Trans>Bungalow</Trans></h2>
                            </div>
                        </div>
                    </div>
                }
                {currCategory === 'House' &&
                    <div className='col-lg'>
                        <div className='position-relative' onClick={() => handleRoute('Condominium')} style={{ cursor: 'pointer' }}>
                            <StaticImage style={{ aspectRatio: '1/1', objectFit: 'cover' }} src="../../../static/assets/asset-landing-3.jpeg" alt="studio type" />
                            <div className='position-absolute w-100 h-100' style={{ top: 0, left: 0, background: 'var(--tertiary)', opacity: .4 }} />
                            <div className='position-absolute' style={{ bottom: '10px', left: '20px' }}>
                                <h2 className='fw-bolder'><Trans>Condominium</Trans></h2>
                            </div>
                        </div>
                        <div className='position-relative mt-4' onClick={() => handleRoute('Cottage')} style={{ cursor: 'pointer' }}>
                            <StaticImage style={{ aspectRatio: '1/1', objectFit: 'cover' }} src="../../../static/assets/asset-landing-3.jpeg" alt="duplex type" />
                            <div className='position-absolute w-100 h-100' style={{ top: 0, left: 0, background: 'var(--tertiary)', opacity: .4 }} />
                            <div className='position-absolute' style={{ bottom: '10px', left: '20px' }}>
                                <h2 className='fw-bolder'><Trans>Cottage</Trans></h2>
                            </div>
                        </div>
                    </div>
                }
            </div>
        </div>
    );
}

export default PropertyType