import React from 'react'
import { StaticImage } from "gatsby-plugin-image"
import { isMobile } from 'react-device-detect';
import { Trans } from 'react-i18next'

const Jumbotron = () => {
    return (
        <div className='position-relative'>
            {isMobile ?
                <StaticImage src={'../../../static/assets/cities-aerial.jpg'} alt='Jumbotron' className='w-100' style={{ aspectRatio: 'unset', height: '70vh', objectFit: 'cover', }} />
                :
                <StaticImage src='../../../static/assets/cities-aerial.jpg' alt='Jumbotron' className='w-100' style={{ aspectRatio: 'unset', maxHeight: '70vh', objectFit: 'cover', }} />
            }            <div style={{ background: 'black', opacity: .45, top: 0 }} className='w-100 h-100 position-absolute' />
            <div className='w-100 h-100 position-absolute d-flex flex-column align-items-center justify-content-center' style={{ zIndex: 10, top: 0 }}>
                <StaticImage src='../../../static/assets/logo.png' alt='Jumbotron Logo Background' className='w-100' style={{ maxWidth: '350px' }} />
                <h1 className='display-3 text-center text-white' style={{fontWeight: '800'}}><Trans>About Us</Trans></h1>
            </div>
        </div>
    );
}

export default Jumbotron