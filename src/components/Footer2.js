import React from "react";
// import { Link } from 'gatsby'
import { StaticImage } from "gatsby-plugin-image" 

const Footer = () => {
    return (
        <div className="">
            <div className="w-100 row m-0" style={{ gap: '10px 0', borderTop: '2px solid #E28800' }}>
                <div className="col-lg-7 bg-tertiary p-0">
                    <div className="p-5">
                        <h1 className="fw-bolder h3 mb-5">Send Us a Message</h1>
                        <div className="d-flex flex-column" style={{ gap: '10px' }}>
                            <input type="text" className="form-control" placeholder="Your Name" />
                            <input type="email" className="form-control" placeholder="Email Address" />
                            <input type="number" className="form-control" placeholder="Phone Number" />
                            <textarea className="form-control" placeholder="Messages" rows="3"></textarea>
                        </div>
                        <div className="d-flex mt-3">
                            <button className="btn btn-secondary py-2 px-5 ms-auto">
                                Send <i className="ti ti-send"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <div className="col-lg-5 p-0">
                    <div className="h-100 w-100 position-relative">
                        <StaticImage className="position-absolute w-100 h-100 bg-secondary" src="../../static/assets/asset-landing.png" alt="A dinosaur"
                            style={{ top: '0', left: '0' }} />
                        <div className="position-absolute w-100 h-100 bg-secondary" style={{ top: '0', left: '0', opacity: '.8' }} />

                        <div className="p-5 position-absolute w-100" style={{zIndex: '9'}}>
                            <h1 className="fw-bolder h3 mb-5 text-white">Contact Information</h1>
                            <div className="d-flex flex-column" style={{ gap: '5px' }}> 
                                <h1 className="fw-bolder h5 text-white">Phone Numbers</h1>
                                <h1 className="h6 fw-normal mb-0 text-white">+959762850296</h1>
                                <h1 className="h6 fw-normal mb-0 text-white">+66900935365</h1>

                                <h1 className="fw-bolder h5 mt-4 text-white">Email</h1>
                                <h1 className="h6 fw-normal mb-0 text-white">info@megacitybangkok.com</h1> 

                                <h1 className="fw-bolder h5 mt-4 text-white">Address</h1>
                                <h1 className="h6 fw-normal mb-0 text-white">10/95, Trendy Office Building, Sukhuvit Soi 13, Khloytoei, Wattana, Bangkok 10110</h1> 
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
            <div className="w-100 d-flex align-items-center flex-wrap justify-content-between container-fluid py-2" style={{ gap: '10px' }}>
                <div className="h6 mb-0" style={{fontSize:'14px'}}>
                    Copyright {new Date().getFullYear()} by MegaCity Bangkok. All Rights Reserved.
                </div>
                <div className="d-flex align-items-center" style={{ gap: '10px' }}>
                    <div className="d-flex bg-primary text-white rounded-circle" style={{ height: '30px', width: '30px' }}>
                        <i className="m-auto ti ti-brand-facebook" style={{ fontSize: '18px' }}></i>
                    </div>
                    <div className="d-flex bg-primary text-white rounded-circle" style={{ height: '30px', width: '30px' }}>
                        <i className="m-auto ti ti-brand-google" style={{ fontSize: '18px' }}></i>
                    </div>
                    <div className="d-flex bg-primary text-white rounded-circle" style={{ height: '30px', width: '30px' }}>
                        <i className="m-auto ti ti-brand-linkedin" style={{ fontSize: '18px' }}></i>
                    </div>
                    <div className="d-flex bg-primary text-white rounded-circle" style={{ height: '30px', width: '30px' }}>
                        <i className="m-auto ti ti-brand-twitter" style={{ fontSize: '18px' }}></i>
                    </div>
                    <div className="d-flex bg-primary text-white rounded-circle" style={{ height: '30px', width: '30px' }}>
                        <i className="m-auto ti ti-brand-youtube" style={{ fontSize: '18px' }}></i>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Footer