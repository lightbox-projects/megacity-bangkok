import React, { useState, useEffect } from 'react'
import ReactPaginate from 'react-paginate';
import { isMobile } from 'react-device-detect';

import PropertyCard from '../Landing/PropertyCard'



const ItemSwiper = ({ properties, location }) => {
    const itemsPerPage = 12
    const [items, setItems] = useState([])
    const [page, setPage] = useState(0)
    const [totalItems, setTotalItems] = useState(0)
    
    useEffect(() => {
        let params = {
            limit: itemsPerPage,
            skip: itemsPerPage * (page),
        }

        for (const [key, value] of Object.entries(location.state)) {
            if(value !== '' && key !== 'key' && key !== 'budget') {
                params = {...params, [key] : value}
            } else if(key === 'budget'){
                let val = value.split(',')
                params = {...params, budget_start: parseInt(val[0]), budget_end: parseInt(val[1]) }
            }
        }
        
        var query = `
        query GetProperties(
                $skip: Int!, $limit: Int!
                ${params.status ? ',$status: String!' : ''}
                ${params.housing_type ? ',$housing_type: String!' : ''}
                ${params.state ? ',$state: String!' : ''}
                ${params.property_size ? ',$property_size: String!' : ''}
                ${params.budget_start ? ',$budget_start: Int!,$budget_end: Int!' : ''}
            ) {
            allMarkdownRemark(
                filter: { 
                    frontmatter: { 
                        ${params.status ? 'status: { eq: $status }' : ''}
                        ${params.housing_type ? 'housing_type: { eq: $housing_type }' : ''}
                        ${params.state ? 'state: { eq: $state }' : ''}
                        ${params.property_size ? 'property_size: { eq: $property_size }' : ''}
                        ${params.budget_start ? 'price: { gte: $budget_start, lte: $budget_end }' : ''}
                    } 
                }
                limit: $limit
                skip: $skip
                ) {
                    nodes {
                        fields {
                            slug
                        }
                        frontmatter {
                            address
                            banner_image
                            bathrooms
                            bedrooms
                            car_port
                            date(formatString: "DD MMMM YYYY")
                            title
                            total_area
                            status
                            housing_type
                            is_featured
                            state
                        }
                    }
                    pageInfo {
                        totalCount
                    }
                }
            }
            `;
            
            fetch('/__graphql', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                },
                body: JSON.stringify({
                    query,
                    variables: {
                        ...params
                    }
                })
            })
            .then(r => r.json())
            .then(data => {
                if (data.data) {
                    setItems(data.data.allMarkdownRemark?.nodes)
                    setTotalItems(data.data.allMarkdownRemark?.pageInfo.totalCount)
                } else setItems([])
            });

    }, [location, page])

    const handlePageClick = (event) => {
        setPage(event.selected)
    };

    return (
        <div className='bg-secondary'>
            <div className={`container-lg ${isMobile ? 'py-4' : 'py-5'}`}>

                <div className={`${isMobile ? '' : 'mt-2'}`}>
                    <div className={isMobile ? 'row px-1' : 'row'} style={{ gap: isMobile ? '10px 0' : '20px 0' }}>
                        {items.map(item => (
                            <div key={item.fields.slug} className={isMobile ? 'col-6 px-1' : 'col-lg-3'}>
                                <PropertyCard minimize={isMobile} properties={item.frontmatter} slug={item.fields.slug} />
                            </div>
                        ))}
                    </div>
                </div> 

                <div className='d-flex mt-5 w-100'>
                    <ReactPaginate
                        className='m-auto'
                        breakLabel="..."
                        nextLabel="Next"
                        onPageChange={handlePageClick}
                        pageRangeDisplayed={5}
                        pageCount={Math.ceil(totalItems / itemsPerPage)}
                        previousLabel="Prev"
                        renderOnZeroPageCount={null}
                    />
                </div>

            </div>
        </div>
    );
}

export default ItemSwiper