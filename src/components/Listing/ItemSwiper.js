import React, { useState, useEffect } from 'react'
import ReactPaginate from 'react-paginate';
import { isMobile } from 'react-device-detect';

import PropertyCard from '../Landing/PropertyCard'
import { Trans, useTranslation } from 'react-i18next'

const ItemSwiper = ({ properties, location }) => {
    const itemsPerPage = 12
    const [items, setItems] = useState([])
    const [page, setPage] = useState(0)
    const [totalItems, setTotalItems] = useState(0)
    const { t } = useTranslation();

    useEffect(() => {
        setPage(0)
    }, [location])

    useEffect(() => {
        let params = {
            limit: itemsPerPage,
            skip: itemsPerPage * (page),
        }

        for (const [key, value] of Object.entries(location.state)) {
            if (value !== '' && key !== 'key' && key !== 'budget') {
                params = { ...params, [key]: value }
            } else if (key === 'budget') {
                let val = value.split(',')
                params = { ...params, budget_start: parseInt(val[0]), budget_end: parseInt(val[1]) }
            }
        }

        let temp = properties.nodes
        // console.log(params)

        if (params.status)
            temp = temp.filter(item => (item.frontmatter.status.includes(params.status)))
        if (params.housing_type)
            temp = temp.filter(item => (item.frontmatter.housing_type === params.housing_type))
        if (params.housing_sub_type)
            temp = temp.filter(item => (item.frontmatter.housing_sub_type === params.housing_sub_type))
        if (params.property_size)
            temp = temp.filter(item => (item.frontmatter.property_size === params.property_size))
        if (params.state)
            temp = temp.filter(item => (item.frontmatter.state === params.state))
        if(params.budget_start){
            temp = temp.filter(item => (item.frontmatter.price >= params.budget_start && item.frontmatter.price <= params.budget_end))
        }

        setTotalItems(temp.length)
        setItems(temp.slice(params.skip, params.skip + itemsPerPage))

    }, [location, page])

    const handlePageClick = (event) => {
        setPage(event.selected)
    };

    return (
        <div className='bg-secondary'>
            <div className={`container-lg ${isMobile ? 'py-4' : 'py-5'}`}>

                <div className={`${isMobile ? '' : 'mt-2'}`}>
                    <div className={isMobile ? 'row px-1' : 'row'} style={{ gap: isMobile ? '10px 0' : '20px 0' }}>
                        {items.map(item => (
                            <div key={item.fields.slug} className={isMobile ? 'col-6 px-1' : 'col-lg-3'}>
                                <PropertyCard minimize={isMobile} properties={item.frontmatter} slug={item.fields.slug} />
                            </div>
                        ))}
                        {items.length < 1 ?
                            <div className='text-center text-white h5'>
                                <Trans>- No Property Available -</Trans>
                            </div>
                        : null}
                    </div>
                </div>

                <div className='d-flex mt-5 w-100'>
                    <ReactPaginate
                        forcePage={page}
                        className='m-auto'
                        breakLabel="..."
                        nextLabel={t('Next')}
                        onPageChange={handlePageClick}
                        pageRangeDisplayed={5}
                        pageCount={Math.ceil(totalItems / itemsPerPage)}
                        previousLabel={t('Previous')}
                        renderOnZeroPageCount={null}
                    />
                </div>

            </div>
        </div>
    );
}

export default ItemSwiper