import * as React from "react"
// import { Link } from "gatsby"

import Navbar from "./Navbar"
import Footer from "./Footer"

const Layout = ({ location, title, children, featured }) => {
  const rootPath = `${__PATH_PREFIX__}/`
  let isRootPath = location.pathname === rootPath

  let splited_pathname = location.pathname.split('/')
  if(['id', 'th'].includes(splited_pathname[1]) && splited_pathname[2] === ""){
    isRootPath = true
  }

  return (
    <div className="global-wrapper" data-is-root-path={isRootPath}>
      <header className="global-header" id="navbar">
        <Navbar isAbsolute={isRootPath} />
      </header>
      <div className="">
        <main>{children}</main>
      </div>
      <footer>
        <Footer featured={featured} />
        {/* © {new Date().getFullYear()}, Built with
        {` `}
        <a href="https://www.gatsbyjs.com">Gatsby</a> */}
      </footer>
    </div>
  )
}

export default Layout
