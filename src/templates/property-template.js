import * as React from "react"
import { graphql } from "gatsby"

import Layout from "../components/layout"
import Seo from "../components/seo"

import Header from '../components/Detail/Header'
import Gallery from '../components/Detail/Gallery'
import Overview from '../components/Detail/Overview'
import AddressFeature from '../components/Detail/AddressFeature'

const DetailPage = ({ data, location }) => {
  const siteTitle = data.site.siteMetadata?.title || `Title` 
  const properties = data.properties.nodes?.[0]
  let featured_properties = data.featured_properties.nodes

  return (
    <Layout location={location} title={siteTitle} featured={featured_properties}> 

      <Header properties={properties} />
      <Gallery properties={properties} />
      <Overview properties={properties} />
      <AddressFeature properties={properties} />

    </Layout>
  )
}

export default DetailPage

/**
 * Head export to define metadata for the page
 *
 * See: https://www.gatsbyjs.com/docs/reference/built-in-components/gatsby-head/
 */
export const Head = () => <Seo title="Landing Page" />

export const pageQuery = graphql`
  query ($language: String!, $id: String!){
    site {
      siteMetadata {
        title
      }
    } 
    locales: allLocale(filter: {language: {eq: $language}}) {
      edges {
        node {
          ns
          data
          language
        }
      }
    }
    properties: allMarkdownRemark(
      filter: {fields: {slug: {regex: "/^/property/"}}, id: {eq: $id}, fileAbsolutePath: {regex: "/property/"}}
      limit: 1
      skip: 0
      sort: {frontmatter: {date: DESC}}
    ) {
      nodes {
        frontmatter {
          address
          banner_image
          bathrooms
          bedrooms
          car_port
          date(formatString: "DD MMMM YYYY")
          features
          zip_code
          title
          total_area
          thumbnails
          status
          state
          property_size
          price
          map_location
          housing_type
          is_featured
        }
        html
      }
    }
    featured_properties: allMarkdownRemark(
      filter: {frontmatter: {is_featured: {eq: true}}, fileAbsolutePath: {regex: "/property/"}}
      limit: 4
      skip: 0
      sort: {frontmatter: {title: ASC}}
    ) {
      nodes {
        fields {
          slug
        }
        frontmatter {
          address
          banner_image
          bathrooms
          bedrooms
          car_port
          date(formatString: "DD MMMM YYYY")
          title
          total_area
          status
          housing_type
          is_featured
          state
        }
      }
    } 
  } 
` 
