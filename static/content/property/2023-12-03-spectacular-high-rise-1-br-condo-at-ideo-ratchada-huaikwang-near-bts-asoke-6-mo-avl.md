---
title: Spectacular High Rise 1-BR Condo at Ideo Ratchada-Huaikwang near BTS
  Asoke | 6 Mo. Avl.
banner_image: /img/1-br_rent-01-536x370.webp
date: 2023-12-03T15:26:04.090Z
housing_type: House
housing_sub_type: Bungalow
property_size: Medium
status:
  - Rent
bathrooms: 1
bedrooms: 1
car_port: 1
total_area: 36.49
price: 15000
address: "-"
state: Angthong
zip_code: "-"
map_location: '{"type":"Point","coordinates":[101.1621094,15.6230368]}'
is_featured: false
---
Charming and cozy1 bedroom condo on the 15th floor of Ideo Ratchada-Huaikwang. It’s located in new CBD areas and It’s in the heart of Ratchadapisek and enjoy night markets, only 1 min from MRT Huaikwang

* Complete range of Sky facilities, including garden, saltwater pool and Shop on premises
* Surrounded by many convenience stores
* Within walking distance of shopping malls, supermarkets, dining restaurants, universities, international hospitals and MRT Huaikwang

The Rama 9-Ratchada area offers one of the most desirable lifestyles in Bangkok, with a great balance between business and residential living, at more moderate price levels compared to districts like Phrom Phong. Located just 3 MRT stops from Sukhumvit Road, Rama 9 and Ratchada districts are lively business and residential areas of the Thai middle class and ex-pats alike. Gleaming high-rise condos, offices, and shopping malls flank the intersections of Ratchadaphisek, Asoke-Din Daeng, Petchaburi and Rama 9, including condos from renowned developers like Noble Revolve by Noble, Ideo Mobi Rama 9 by Ananda, and Life Asoke-Rama 9 by AP. Living in a condominium in the Rama 9 neighborhood is a great trade-off between price, quality of life and proximity to downtown Bangkok. There is a huge selection of amazing food options, cafes, music venues. As for shopping, Central Plaza Grand Rama IX, Fortune Town, Esplanade Department store or the legendary Ratchada Night market (aka Train Market) are popular options in the Rama 9-Ratchada area.

Monthly rent depending on minimum length of lease

* Min. 1-3 months lease: THB 18,000/month
* Min. 6 months lease: THB 17,000/month
* Min. 12 months lease: THB 15,000/month

FlexstayRentals is your trusted EXPERT for residential rentals in Bangkok starting from 1 month with more than 3,000 attractive units in our portfolio.

Our services for you:

Multilingual service centre to help with your inquiries and during your stay

Simple, standard lease agreement

Trusted deposit handling

Professional check-in/checkout service

Additional services like cleaning upon request

Contact us today to find your dream home!