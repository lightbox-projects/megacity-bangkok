---
title: Spacious 1-BR Condo at Supalai Place Sukhumvit 39 near BTS Phrom Phong |
  Min. Lease 6 Mth
banner_image: /img/spacious-1-br-condo-၀၁-536x370.webp
date: 2023-12-03T15:24:48.802Z
housing_type: House
housing_sub_type: Condominium
property_size: Medium
status:
  - Rent
bathrooms: 1
bedrooms: 1
car_port: 1
total_area: 51
price: 16500
address: "-"
state: Amnatcharoen
zip_code: "-"
map_location: '{"type":"Point","coordinates":[101.5136719,15.1145529]}'
is_featured: true
---
Contemporary and clean-design 1 bedroom condo on the 7 th floor of the Supalai Place Sukhumvit 39. It’s located in prime business areas and It’s in the heart of Sukhumvit, only 8 mins from BTS Phrom Phong

* Complete range of facilities, including Sky Garden and Saltwater Pool
* Surrounded by many convenience stores
* Within walking distance of shopping malls, supermarkets, dining restaurants, universities, international hospitals and BTS Phrom Phong

Phrom Phong is an international, upmarket district famous for its luxury malls, interior design stores and fashion outlets. It has an immense variety of food options stretching out around Sukhumvit Road, from local food stalls and markets to fine European and Asian restaurants. Phrom Phong is also the shopping heaven of Bangkok with glamorous shopping malls such as EmQuartier and Emporium right next to the BTS station. Last but not least, Phrom Phong is also a glamorous nightlife spot with cool bars, clubs and karaoke places to choose from.

Monthly rent depending on minimum length of lease

* Min. 12 months lease: THB 16,500/month

FlexstayRentals is your trusted EXPERT for residential rentals in Bangkok starting from 1 month with more than 3,000 attractive units in our portfolio.

Our services for you:

Multilingual service centre to help with your inquiries and during your stay

Simple, standard lease agreement

Trusted deposit handling

Professional check-in/checkout service

Additional services like cleaning upon request

Contact us today to find your dream home!