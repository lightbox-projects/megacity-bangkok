---
title: Level Bang Na Condo
banner_image: /img/level_bang_na01-536x370.jpg
date: 2023-12-03T15:10:05.237Z
housing_type: Apartement
housing_sub_type: Convertible
property_size: Medium
status:
  - Sale
thumbnails:
  - /img/level_bang_na01-536x370.jpg
bathrooms: 1
bedrooms: 2
car_port: 1
total_area: 40
features: []
price: 2000000
address: --
state: Amnatcharoen
zip_code: --
map_location: '{"type":"Point","coordinates":[103.2714844,17.1407904]}'
is_featured: false
---
Ready to move in, fully furnished, 2 beds, 40sqm, the special price is 2 million THB. down payment 30%, within 1 month, transfer ownership finished the rest 70% payment