---
title: Wonderful High Rise 1-BR Condo at Fuse Chan-Sathorn near BTS Surasak
banner_image: /img/wonderful_high_rise_1-br15-536x370.webp
date: 2023-12-02T11:57:53.054Z
housing_type: Apartement
housing_sub_type: Duplex
property_size: Medium
status:
  - Sale
thumbnails:
  - /img/wonderful_high_rise_1-br01-536x370.webp
  - /img/wonderful_high_rise_1-br20-536x370.webp
  - /img/wonderful_high_rise_1-br19-536x370.webp
  - /img/wonderful_high_rise_1-br03-536x370.webp
  - /img/wonderful_high_rise_1-br04-536x370.webp
  - /img/wonderful_high_rise_1-br05-536x370.webp
  - /img/wonderful_high_rise_1-br06-536x370.webp
  - /img/wonderful_high_rise_1-br07-536x370.webp
  - /img/wonderful_high_rise_1-br08-536x370.webp
  - /img/wonderful_high_rise_1-br09-536x370.webp
  - /img/wonderful_high_rise_1-br10-536x370.webp
  - /img/wonderful_high_rise_1-br11-536x370.webp
  - /img/wonderful_high_rise_1-br12-536x370.webp
  - /img/wonderful_high_rise_1-br13-536x370.webp
  - /img/wonderful_high_rise_1-br14-536x370.webp
  - /img/wonderful_high_rise_1-br16-536x370.webp
  - /img/wonderful_high_rise_1-br17-536x370.webp
  - /img/wonderful_high_rise_1-br18-536x370.webp
bathrooms: 1
bedrooms: 1
car_port: 1
total_area: 35.02
features:
  - Gym
  - Relaxing swimming pool
  - Security cameras
  - 24-hours security
  - Open car park
  - Covered car park
  - Parking space available
  - Fitness corner
price: 3250000
address: --
state: Satun
zip_code: "00000"
map_location: '{"type":"Point","coordinates":[105.6481877,11.2799106]}'
is_featured: true
---
Fully furnished 1 bedroom, 1 bathroom condo for sale with a floorsize of 35.05 squaremeters, located on the 26th floor of the Fuse Chan Sathorn building, in the popular Sathon district near BTS Surasak. Please contact us to schedule a viewing.

Unit amenities:

* Building completed in 2015
* Balcony