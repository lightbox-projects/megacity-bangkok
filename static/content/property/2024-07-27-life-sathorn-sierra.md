---
title: Life Sathorn-Sierra
banner_image: /img/02_room_1_2.jpg
date: 2024-07-27T07:32:34.918Z
housing_type: Apartement
housing_sub_type: Condominium
property_size: Small
status:
  - Sale
thumbnails:
  - /img/02_room_1_8.jpg
bathrooms: 1
bedrooms: 1
car_port: 1
total_area: 32.94
price: 3658600
address: Ratchaphruek Road, Bukkhalo Subdistrict, Thonburi District, Bangkok 10600
state: Bangkok
zip_code: --
map_location: '{"type":"Point","coordinates":[-42.8707008,53.8769143]}'
is_featured: false
---
### High Rise Condo 40 floors high, standing out in terms of its outstanding location, in the heart of Sathorn-Tha Phra intersection, combining a new lifestyle with a common area design inspired by 6 great natural resources of the world* on an area of over 5 rai, only 150 m. to BTS Talat Phlu