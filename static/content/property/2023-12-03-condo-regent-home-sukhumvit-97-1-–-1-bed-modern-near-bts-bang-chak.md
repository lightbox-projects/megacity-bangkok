---
title: Condo Regent Home Sukhumvit 97/1 – 1 bed modern near BTS Bang Chak
banner_image: /img/condo-ideo-ratchada-01-536x370.webp
date: 2023-12-03T15:19:14.924Z
housing_type: House
housing_sub_type: Single-Family
property_size: Medium
status:
  - Sale
bathrooms: 1
bedrooms: 1
car_port: 1
total_area: 32
price: 1390000
address: "-"
state: Ayutthaya
zip_code: --
map_location: '{"type":"Point","coordinates":[100.8105469,16.6361919]}'
is_featured: false
---
Fully furnished condo, 1 bedroom, 1 bathroom, for rent / for sale, room size 32 square meters, 8th floor, Regent Home Sukhumvit 97 1 project, located in Phra Khanong area, near BTS Bang Chak. Contact us to make an appointment to view the items you want.

Facilities information:

* Completed in 2019

Common area:

* 24 hour security system
* swimming pool
* closed-circuit camera
* garden
* gym

The monthly rent is based on the minimum lease term:

* Minimum 12-month lease: ฿10,000 per month.