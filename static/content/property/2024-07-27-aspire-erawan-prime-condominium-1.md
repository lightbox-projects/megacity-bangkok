---
title: Aspire Erawan Prime Condominium
banner_image: /img/1-bed-27-sq.m.jpg
date: 2024-07-27T04:21:52.931Z
housing_type: Apartement
housing_sub_type: Condominium
property_size: Small
status:
  - Sale
bathrooms: 1
bedrooms: 1
car_port: 1
total_area: 27.07
price: 2048930
address: Sukhumwith Road, Pak Nam, Mueang Samut Prakan, Samut Prakan 10270
state: Bangkok
zip_code: "10270"
map_location: '{"type":"Point","coordinates":[28.2349644,22.60433]}'
is_featured: false
---
1 Building ,30 storeys 

1st Floor:    Wellcome Zone ,Private Lobby ,Meeting Reservation Room , CO-Working Café , Mingle Space , Retail shop, public restroom, Mail room, Maid room, Security room, Laundry room, Control room Generator room, MDB room, garbage room, garden and parking 

2nd Floor : AHU room and parking 

3rd - 6th Floor : Residential Units and parking 

7th Floor : Residential Units, Fitness Room, Saperated male and female steam room, Lounge, Swimming Pool and Garden 

8th-30th Floor : Residential Units 

Rooftop : Roof, pump room, water tank, lift engine room, rooftop emergency exit area     

T﻿ransportation:  - BTS Chang Erawan (0M),             

\- Chalerm Maha Makhon Expressway (6.2 km)

\- Kanchanaphisek Rd (700m)

\- Chalong Rat Expressway (9 km)