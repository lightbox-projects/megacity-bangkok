---
title: Rhythm Ekkamai Estate
banner_image: /img/rhythm-ekkamai-estate_sky.jpg
date: 2024-07-27T08:15:13.740Z
housing_type: Apartement
housing_sub_type: Condominium
property_size: Large
status:
  - Sale
thumbnails: []
bathrooms: 2
bedrooms: 2
car_port: 1
total_area: 90.33
price: 26359998
address: Sukhumvit 63 (Ekkamai) Road, Khlong Tan Nuea, Watthana, Bangkok
state: Bangkok
zip_code: --
map_location: '{"type":"Point","coordinates":[-42.8707008,53.8769143]}'
is_featured: false
---


When inspirations from the old homes of Ekkamai meet the “Feel Like Home” design concept, the result is a NEW VERTICAL HOME where “Luxury” and “Homey” are perfectly blended to recreate the storied charms of Ekkamai living of a bygone era. A unique, 7-storey-high elevated lobby replicates the sense of ascending to the top floor of your home, with the balcony-like Main Lobby located on the 7th floor.

Only 180 meters from Thonglor Soi 10, it is a location that easily connects to Thonglor Soi. It is [close to Ekkamai BTS station,](https://www.homenayoo.com/condo-bts-ekkamai/) surrounded by hundreds of restaurants and many large shopping malls such as Donki Thonglor, Big C Ekkamai , Gateway Ekamai, Major Ekkamai, Emporium, Emquartier, Terminal 21, within walking distance to BTS station only 2-3 stations.