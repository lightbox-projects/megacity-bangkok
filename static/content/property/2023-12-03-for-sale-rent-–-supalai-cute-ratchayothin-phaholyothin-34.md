---
title: For Sale/Rent – Supalai Cute Ratchayothin-Phaholyothin 34
banner_image: /img/supalai_cute_ratchayothin-phaholyothin02-536x370.jpg
date: 2023-12-03T15:17:23.431Z
housing_type: House
housing_sub_type: Single-Family
property_size: Medium
status:
  - Rent
  - Sale
bathrooms: 1
bedrooms: 1
car_port: 1
total_area: 48
price: 3900000
address: --
state: Amnatcharoen
zip_code: --
map_location: '{"type":"Point","coordinates":[100.8105469,14.7748825]}'
is_featured: false
---
## 1 Bedroom Condo for Sale or Rent in Supalai Cute Ratchayothin – Phaholyothin34, Sena Nikhom, Bangkok near BTS Kasetsart University

For Sale/Rent – Supalai Cute Ratchayothin-Phaholyothin 34\
Location :\
Soi Phaholyothin 34 ( sena 2 ) Phaholyothin Road, Kwaeng Senanikom, Khet Chatujak, Bangkok 10900\
For Sale – FREEHOLD One Bedroom Deluxe Suite 48.5 sqm Corner unit on the 4th Floor of Aster Building of Supalai Cute Ratchayothin-Phaholyothin 34.\
Fully furnished with fingerprint-scanner door access . Ready to move in.\
Facilities :\
Infinity Edge Swimming Pool\
Fitness\
Garden\
Kids Playground\
24/7 Security guard\
CCTV\
Nearby places :\
BTS Senanikom\
Kasetsart university\
Donmuang Airport\
The outstanding amount to be paid on ownership transfer date is : 3,558,014 mill. Baht.\
Please call/ 

View Phone

 for viewing arrangement