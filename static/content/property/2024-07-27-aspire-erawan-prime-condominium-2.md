---
title: Aspire Erawan Prime Condominium
banner_image: /img/2-bed.jpg
date: 2024-07-27T04:24:12.862Z
housing_type: Apartement
housing_sub_type: Condominium
property_size: Medium
status:
  - Sale
bathrooms: 1
bedrooms: 2
car_port: 1
total_area: 46.08
price: 3649393
address: Sukhumwith Road, Pak Nam, Mueang Samut Prakan, Samut Prakan 10270
state: Bangkok
zip_code: "10270"
map_location: '{"type":"Point","coordinates":[28.2349644,22.60433]}'
is_featured: false
---
1 Building ,30 storeys 

1st Floor:    Wellcome Zone ,Private Lobby ,Meeting Reservation Room , CO-Working Café , Mingle Space , Retail shop, public restroom, Mail room, Maid room, Security room, Laundry room, Control room Generator room, MDB room, garbage room, garden and parking 

2nd Floor : AHU room and parking 

3rd - 6th Floor : Residential Units and parking 

7th Floor : Residential Units, Fitness Room, Saperated male and female steam room, Lounge, Swimming Pool and Garden 

8th-30th Floor : Residential Units 

Rooftop : Roof, pump room, water tank, lift engine room, rooftop emergency exit area     

T﻿ransportation:  - BTS Chang Erawan (0M),             

\- Chalerm Maha Makhon Expressway (6.2 km)

\- Kanchanaphisek Rd (700m)

\- Chalong Rat Expressway (9 km)