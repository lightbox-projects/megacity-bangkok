---
title: Beautiful 2-BR Condo at Lumpini Center Sukhumvit 77 near BTS On Nut |
  Min. Lease 6 Mth
banner_image: /img/beautiful_2-br_condo01-536x370.webp
date: 2023-12-03T15:22:45.116Z
housing_type: House
housing_sub_type: Single-Family
property_size: Medium
status:
  - Rent
thumbnails: []
bathrooms: 1
bedrooms: 2
car_port: 1
total_area: 57
price: 14000
address: "-"
state: Amnatcharoen
zip_code: "-"
map_location: '{"type":"Point","coordinates":[101.1621094,15.199386]}'
is_featured: true
---
#### Fully furnished 2-bedroom Condo on 4th floor of the Lumpini Center Sukhumvit 77, with facilities, located in the heart of the city in Onnut district, within walking distance from BTS, perfect for urban lifestyle.

* Fully furnished with stylish interior
* Fast Wifi (200 MBPS)
* Amazing pool area
* Modern high-end gym
* Within walking distance of shopping malls, supermarkets, dining restaurants, international hospitals, and BTS Phrom Phong

#### Onnut is a charming and increasingly popular residential area, liked both by locals and expats. It’s known to be a peaceful, authentic district with amazing food options. Affordable rental prices, a short distance and convenient access to Bangkok downtown, and a great selection of local amenities make On Nut a popular choice. People who work remotely will find the co-working spaces in the area attractive.

* Monthly rent depending on minimum length of lease
* Min. 1-month lease: THB 17,000/month
* Min. 6-months lease: THB 16,000/month
* Min. 12-months lease: THB 14,000/month

#### FlexstayRentals is your trusted EXPERT for residential rentals in Bangkok starting from 1 month with more than 3,000 attractive units in our portfolio.

#### Our services for you:

* Multilingual service centre to help with your inquiries and during your stay
* Simple, standard lease agreement
* Trusted deposit handling
* Professional checkin/checkout service
* Additional services like cleaning upon request